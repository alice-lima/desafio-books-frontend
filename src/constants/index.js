export const BASE_URL = "https://books.ioasys.com.br/api/v1";

export const LOCAL_STORAGE_PREFIX = "@books:"

export const TOKEN_KEY = `access_token`;
export const REFRESH_TOKEN_KEY = `refresh_access_token`;
