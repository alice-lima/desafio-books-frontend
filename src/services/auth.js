import { REFRESH_TOKEN_KEY, TOKEN_KEY } from "../constants";
import api from "./index";
import { clearLocalStorage, setStoragedItem } from "./localStorage";

export const login = async (data) => {
    try {
        const response = await api.post('/auth/sign-in', data);

        const headers = response.headers;

        setStoragedItem(TOKEN_KEY, headers["authorization"]);
        setStoragedItem(REFRESH_TOKEN_KEY, headers["refresh-token"]);
        
        return response.data;

    } catch (error) {
        console.error("Error on login: ", error);
        return null;
    }
}

export const logout = () => {
    clearLocalStorage();
}