import { LOCAL_STORAGE_PREFIX } from "../constants"

export const getStoragedItem = (key) => {
    const value = localStorage.getItem(`${LOCAL_STORAGE_PREFIX}${key}`);
    return value;
}

export const setStoragedItem = (key, value) => {
    if(!value) return;

    const parsedValue = typeof value === 'string' ? value :  JSON.parse(value);
    
    localStorage.setItem(`${LOCAL_STORAGE_PREFIX}${key}`, parsedValue);
}

export const clearLocalStorage = () => {
    localStorage.clear()
}