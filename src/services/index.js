import axios from "axios";
import { BASE_URL, TOKEN_KEY, REFRESH_TOKEN_KEY } from "../constants";
import { getStoragedItem, setStoragedItem } from "../services/localStorage";

const instance = axios.create({
  baseURL: BASE_URL,
  "Content-Type": "application/json",
  timeout: 5000,
});

// Add a request interceptor
instance.interceptors.request.use(
  (config) => {
    const token = getStoragedItem(TOKEN_KEY);
    const refresh_token = getStoragedItem(REFRESH_TOKEN_KEY);

    if (!token) return config;
   
    const bearer = `Bearer ${
      config.url.includes('/auth/refresh-token') ? refresh_token : token
    }`;
  
    config.headers["Authorization"] = bearer;
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
 );
  
 instance.interceptors.response.use(
  (response) => {
    return response;
  },
  function (error) {
    if (error?.response?.status !== 401) return Promise.reject(error);
  
    const originalRequest = error.config;

    const refreshToken = getStoragedItem(REFRESH_TOKEN_KEY);
  
    if (
      error.response.status === 401 &&
      originalRequest.url === '/auth/refresh-token'
    ) {
      window.location.pathname = '/logout';
      return Promise.reject(error);
    }
  
    if (!error?.response?.status === 401 || originalRequest._retry)
      return Promise.reject(error);
  
    originalRequest._retry = true;
  
    return instance.post('/auth/refresh-token', { refreshToken })
    .then(({ status, headers }) => {
      if (status === 204) {
        const token = headers["authorization"];
        const refresh_token = headers["refresh-token"];

        setStoragedItem(TOKEN_KEY, token);
        setStoragedItem(REFRESH_TOKEN_KEY, refresh_token);
        setTokenOnHeader(token);

        return instance(originalRequest);
      }
    })
    .catch(error => {
      console.error('Error on refresh token', error);
      return Promise.reject(error);
    });
  }
 );

 export const setTokenOnHeader = token => {
  instance.defaults.headers.common.Authorization = `Bearer ${token}`;
 };
 

export default instance;