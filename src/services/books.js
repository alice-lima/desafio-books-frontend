import api from "./index";

export const getBooks = async (page, amount) => {
    try {
        const response = await api.get('/books', { params: { page, amount } });
        
        return response.data;

    } catch (error) {
        console.error("Error on get books: ", error);
        return null;
    }
}

export const getBookById = async (bookId) => {
    try {
        const response = await api.get(`/books/${bookId}`);
        
        return response.data;

    } catch (error) {
        console.error("Error on get book by id: ", error);
        return null;
    }
}