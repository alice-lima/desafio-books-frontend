import Routes from "./routes";
import AppProvider from './hooks';
import { GlobalStyle } from "./components/GlobalStyle";

function App() {
  return (
    <AppProvider>
      <GlobalStyle />
      <Routes />
    </AppProvider>
  );
}

export default App;
