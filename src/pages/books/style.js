import styled from "styled-components";
import bgImage from "../../assets/images/background-image-home.png";

export const Container = styled.div`
    width: 100%;
    height: auto;
    min-height: 100%;
    
    background-image: url(${bgImage});
    background-size: cover;
    background-blend-mode: darken;

    @media(max-width: 768px) {
    }
`;

export const Content = styled.div`
    display: flex;
    flex-direction: column;
    padding: 44px 120px;
    gap: 40px;
        
    @media (max-width: 768px) {
        padding: 42px 12px;
    }
`;