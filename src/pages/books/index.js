import React, { useEffect, useState } from "react";
import BooksList from "../../components/BooksList";
import Header from "../../components/Header";
import Pagination from "../../components/Pagination";
import { useAuth } from "../../hooks/auth";
import { getBooks } from "../../services/books";
import { Container, Content } from "./style";

const Books = () => {
    const [books, setBooks] = useState([]);
    const [activePage, setActivePage] = useState(1);
    const [numTotalPages, setNumTotalPages] = useState(1);

    const { loggedUser } = useAuth();

    useEffect(() => {
        async function getData(activePage) {
            const response = await getBooks(activePage, 12);

            setBooks(response.data);
            setNumTotalPages(Math.ceil(response.totalPages));
        }

        getData(activePage);
    }, [activePage])

    return (
        <Container>
            <Content>
                <Header user={loggedUser} />
                <BooksList 
                    data={books} 
                />

                <Pagination 
                    activePage={activePage} 
                    numTotalPages={numTotalPages}
                    handleActivePage={setActivePage}
                />
            </Content>
        </Container>
    )
}

export default Books;