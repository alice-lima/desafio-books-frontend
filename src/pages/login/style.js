import styled from "styled-components";
import backgroundImage from "../../assets/images/background-image.png";

export const Background = styled.div`
    background: url(${backgroundImage}) no-repeat;
    background-size: cover;
    display: flex;
    height: 100%;

    justify-content: flex-start;
    align-items: center;
    padding: 0 115px;
    
    @media(max-width: 768px) {
        background-position: top;
        align-items: center;
        padding: 16px;
    }
`;

export const Block = styled.div`
    display: flex;
    flex-direction: column;
    flex: 1;
    max-width: 350px;

    @media(max-width: 768px) {
        max-width: 100%;
    }
`;