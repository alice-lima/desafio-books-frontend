import React from "react";
import FormLogin from "../../components/FormLogin";
import Title from "../../components/Title";
import { Background, Block } from "./style";

const Login = () => {

    return (
        <Background>
            <Block>
                <Title isLight />
                <FormLogin />
            </Block>
        </Background>
    )
}

export default Login;