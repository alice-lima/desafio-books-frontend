import React, {
    createContext,
    useCallback,
    useState,
    useContext,
    useEffect,
  } from 'react';
import {LOCAL_STORAGE_PREFIX} from '../constants';

const LOGGED_USER = `${LOCAL_STORAGE_PREFIX}loggedUser`;

const AuthContext = createContext({});

const AuthProvider = ({children}) => {
  const [isLoggedIn, _setIsLoggedIn] = useState(false);
  const [loggedUser, _setLoggedUser] = useState(null);

  useEffect(() => {
    async function loadStorageData() {
      const storedLoggedUser = localStorage.getItem(
        LOGGED_USER,
      );

      if (storedLoggedUser) {
        _setLoggedUser(JSON.parse(storedLoggedUser));
        _setIsLoggedIn(true);
      }
    }

    loadStorageData();
  }, []);

  const setLoggedUser = useCallback(async (user) => {
    localStorage.setItem(
      LOGGED_USER,
      JSON.stringify(user),
    );

    _setLoggedUser(user);
    _setIsLoggedIn(true);
  }, []);

  return (
    <AuthContext.Provider
      value={{
        loggedUser,
        isLoggedIn,
        setLoggedUser,
      }}>
      {children}
    </AuthContext.Provider>
  );
};

function useAuth() {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth must be used within a AuthProvider');
  }

  return context;
}

export {AuthContext, AuthProvider, useAuth};
  