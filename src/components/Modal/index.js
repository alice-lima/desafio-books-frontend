import React, { useMemo } from "react";
import { formatterWritersName } from "../../utils/formatterWritersName";
import closeIcn from "../../assets/images/close-icn.png";
import defaultImage from "../../assets/images/image-placeholder.png";
import { 
    Background, 
    Container,
    Column,
    BookImage,
    BookName,
    BookWriters,
    LineData,
    BookTitleData,
    BookData,
    BookDataQuotation,
    ButtonClose,
} from "./style";

const Modal = ({ data, handleClose }) => {
    const formattedWriters = useMemo(() => {
        return formatterWritersName(data.authors)
    }, [data])

    return (
        <Background>
            <ButtonClose onClick={() => handleClose()}>
                <img src={closeIcn} alt="Fechar" />
            </ButtonClose>
            <Container>
                <Column centered>
                    <BookImage
                        src={data.imageUrl || defaultImage} 
                        alt={`Capa do livro ${data.title}`}
                    />
                </Column>
                <Column>
                    <Column>
                        <BookName>{data.title}</BookName>
                        <BookWriters>
                            {formattedWriters}
                        </BookWriters>
                    </Column>
                    
                    <Column>
                        <LineData>
                            <BookTitleData>
                                INFORMAÇÕES
                            </BookTitleData>
                        </LineData>
                        <LineData>
                            <BookTitleData>
                                Páginas
                            </BookTitleData>
                            <BookData>
                                {data.pageCount}
                            </BookData>
                        </LineData>
                        <LineData>
                            <BookTitleData>
                                Editora
                            </BookTitleData>
                            <BookData>
                                {data.publisher}
                            </BookData>
                        </LineData>
                        <LineData>
                            <BookTitleData>
                                Publicação
                            </BookTitleData>
                            <BookData>
                                {data.published}
                            </BookData>
                        </LineData>
                        <LineData>
                            <BookTitleData>
                                Idioma
                            </BookTitleData>
                            <BookData>
                                {data.language}
                            </BookData>
                        </LineData>
                        <LineData>
                            <BookTitleData>
                                Título Original
                            </BookTitleData>
                            <BookData>
                                {data.title}
                            </BookData>
                        </LineData>
                        <LineData>
                            <BookTitleData>
                                ISBN-10
                            </BookTitleData>
                            <BookData>
                                {data.isbn10}
                            </BookData>
                        </LineData>
                        <LineData>
                            <BookTitleData>
                                ISBN-13
                            </BookTitleData>
                            <BookData>
                                {data.isbn13}
                            </BookData>
                        </LineData>
                    </Column>


                    <Column>
                        <LineData>
                            <BookTitleData>RESENHA DA EDITORA</BookTitleData>
                        </LineData>

                        <LineData>
                            <BookDataQuotation>
                                {/* <svg>
                                    <image href={quotationMarkIcn} />
                                </svg> */}

                                {data.description}
                            </BookDataQuotation>
                        </LineData>
                    </Column>
                </Column>
            </Container>
        </Background>
    )
};

export default Modal;