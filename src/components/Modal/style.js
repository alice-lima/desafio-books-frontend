import styled, { css } from "styled-components";
import { booksDataText, writersText } from "../../constants/colors";


export const Background = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    height: 100%;
    background: rgba(0, 0, 0, 0.4);
    backdrop-filter: blur(2px);
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-end;
    padding: 16px;
    gap: 24px;
    width: -webkit-fill-available;
    
    @media(max-width: 768px) {
        gap: 16px;
        max-height: -webkit-fill-available;
    }
`;

export const Container = styled.div`
    background: #FFFFFF;
    border-radius: 4px;
    padding: 24px;
    display: flex;
    gap: 48px;
    max-width: 768px;
    align-self: center;
    
    @media(max-width: 768px) {
        gap: 0;
        overflow: scroll;
        flex-direction: column;
    }
`;

const centeredColumn = css`
    justify-content: center;
    align-items: center;
`;

export const Column = styled.div`
    display: flex;
    flex-direction: column;
    padding: 16px 0;
    
    /* flex: ${({ isSmall }) => isSmall ? 1 : 2}; */

    ${({ centered }) => centered && centeredColumn}

    @media(max-width: 768px) {

    }
`;

export const BookImage = styled.img`
    height: 100%;
    max-width: 350px;
    max-height: 512px;
    filter: drop-shadow(0px 11.9008px 17.8512px rgba(0, 0, 0, 0.3));
    
    @media(max-width: 768px) {
        max-width: 240px;
        max-height: 350px;
    }
`;

export const BookName = styled.span`
    font-weight: 500;
    font-size: 28px;
    line-height: 40px;

    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
`;

export const BookWriters = styled.span`
    font-weight: 400;
    font-size: 12px;
    line-height: 20px;

    color: ${writersText};
`;

export const LineData = styled.div`
    display: flex;
    justify-content: space-between;
`;

export const BookTitleData = styled.span`
    font-weight: 500;
    font-size: 12px;
    line-height: 28px;
`;

export const BookData = styled.span`
    font-weight: 400;
    font-size: 12px;
    line-height: 20px;

    color: ${booksDataText};
`;

export const BookDataQuotation = styled(BookData)`
    &::before {
        content: open-quote;
        display: inline-block;
        font-family: initial;
        font-size: 30px;
        vertical-align: -webkit-baseline-middle;
        margin-right: 5px;
    }
`;

export const ButtonClose = styled.button`
    background: #FFFFFF;
    border: 1px solid rgba(51, 51, 51, 0.2);
    box-sizing: border-box;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    width: 32px;
    height: 32px;
    padding: 10px;

    img {
        width: 12px;
        height: 12px;
    }
`;