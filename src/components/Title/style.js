import styled from "styled-components";
import { primaryText } from "../../constants/colors";

export const Text = styled.h1`
    font-style: normal;
    font-weight: 300;
    font-size: 28px;
    line-height: 40px;
    display: flex;
    align-items: center;
    gap: 12px;

    color: ${({ isLight }) => isLight ? "#fff" : primaryText};
`;

export const Logo = styled.img`
`;