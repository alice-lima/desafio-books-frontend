import React from "react";
import logo from "../../assets/images/logo.png";
import logoDark from "../../assets/images/logo-dark.png";
import { Text, Logo } from "./style";

const Title = ({ isLight }) => {
    return (
        <Text isLight={isLight}>
            <Logo src={isLight ? logo : logoDark} alt="Logo ioasys" />
            Books
        </Text>
    )
};

export default Title;