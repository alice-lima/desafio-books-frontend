import styled from "styled-components";

export const Container = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-template-rows: auto;
    grid-column-gap: 16px;
    grid-row-gap: 16px;
    
    @media (max-width: 768px) {
        display: flex;
        flex-wrap: wrap;
        flex-direction: column;
        gap: 16px;
    }
`;