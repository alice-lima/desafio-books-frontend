import React, { useState, useCallback } from "react";
import { getBookById } from "../../services/books";
import CardList from "../CardList";
import Modal from "../Modal";
import { Container } from "./style";

const BooksList = ({ data }) => {
    const [isShowingModal, setIsShowingModal] = useState(false);
    const [openedBook, setOpenedBook] = useState(null);

    const openBook = useCallback(async bookId => {
        const book = await getBookById(bookId);

        setOpenedBook(book);
        setIsShowingModal(true);
    }, []);

    const closeBook = useCallback(() => {
        setIsShowingModal(false);
        setOpenedBook(null);
    }, []);

    return (
        <Container>
            {data?.length > 0 && data.map(book => {
                return (
                    <CardList 
                        key={book.id} 
                        item={book} 
                        onClick={() => openBook(book.id)} 
                    />
                )
            })}

            {isShowingModal && (
                <Modal data={openedBook} handleClose={closeBook} />
            )}

        </Container>
    )
}

export default BooksList;