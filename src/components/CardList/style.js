import styled from "styled-components";
import { booksDataText, writersText } from "../../constants/colors";

export const Container = styled.div`
    display: flex;
    background: #FFFFFF;
    box-shadow: 0px 6px 24px rgba(84, 16, 95, 0.13);
    border-radius: 4px;
    padding: 19px 16px;
    cursor: pointer;
    gap: 10px;


    @media (max-width: 768px) {
        
    }
`;

export const Column = styled.div`
    display: flex;
    flex-direction: column;
    
    flex: ${({ isSmall }) => isSmall ? 1 : 2};
`;

export const BookImage = styled.img`
    height: 100%;
    max-height: 120px;
    max-width: 80px;
    filter: drop-shadow(0px 6px 9px rgba(0, 0, 0, 0.15));
`;

export const BookName = styled.span`
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
`;

export const BookWriters = styled.span`
    font-weight: 400;
    font-size: 12px;
    line-height: 20px;

    color: ${writersText};
`;

export const BookData = styled.span`
    font-weight: 400;
    font-size: 12px;
    line-height: 20px;

    color: ${booksDataText};
`;