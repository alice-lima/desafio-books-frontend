import React, { useMemo } from "react";
import { formatterWritersName } from "../../utils/formatterWritersName";
import defaultImage from "../../assets/images/image-placeholder.png";
import { 
    Container,
    Column, 
    BookImage, 
    BookName, 
    BookWriters, 
    BookData 
} from "./style";

const CardList = ({ item, onClick }) => {
    const formattedWriters = useMemo(() => {
        return formatterWritersName(item.authors)
    }, [item])

    return (
        <Container onClick={() => onClick(item)}>
            <Column isSmall>
                <BookImage 
                    src={item.imageUrl || defaultImage} 
                    alt={`Capa do livro ${item.title}`}
                />
            </Column>
            <Column>
                <Column>
                    <BookName>
                        {item?.title}
                    </BookName>
                    <BookWriters>
                        {formattedWriters}
                    </BookWriters>
                </Column>

                <Column>
                    <BookData>
                        {`${item?.pageCount} páginas`}
                    </BookData>
                    <BookData>
                        {`Editora ${item?.publisher}`}
                    </BookData>
                    <BookData>
                        {`Publicado em ${item?.published}`}
                    </BookData>
                </Column>
            </Column>
        </Container>
    )
};

export default CardList;