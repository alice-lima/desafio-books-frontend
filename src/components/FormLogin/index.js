import React, { useCallback, useEffect, useState } from "react";
import Input from "../Input";
import ErrorMessage from "../ErrorMessage";
import LoginButton from "../LoginButton";
import { Form } from "./style";
import { login } from "../../services/auth";
import { useAuth } from "../../hooks/auth";
import { useNavigate } from "react-router-dom";

const FormLogin = () => {
    const [user, setUser] = useState(null);
    const [isError, setIsError] = useState(false);

    const navigate = useNavigate();

    const { setLoggedUser, loggedUser } = useAuth();

    const submitLogin = useCallback(async (e, data) => {
        e.preventDefault();
        const response = await login(data);

        
        if(!response) {
           return setIsError(true);
        }

        setIsError(false);
        setLoggedUser(response);

    }, [])
    
    useEffect(() => {
        if(loggedUser) {
            navigate("/books", { replace: true });
        }
    }, [loggedUser]);

    return (
        <Form onSubmit={(e) => submitLogin(e, user)}>
            <Input 
                id="txtEmail" 
                label="Email" 
                type="email" 
                onChange={(e) => setUser({ ...user, email: e.target.value})}
            />
            <Input 
                required
                id="txtPassword" 
                label="Senha" 
                type="password" 
                onChange={(e) => setUser({ ...user, password: e.target.value})}
            >
                <LoginButton label="Entrar" type="submit" />
            </Input>

            {isError && (
                <ErrorMessage text="Email e/ou senha incorretos." />
            )}
        </Form>
    )
}

export default FormLogin;