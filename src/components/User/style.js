import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    gap: 15px;
    align-items: center;

    @media(max-width: 768px) {
        span {
            display: none;
        }
    }
`;

export const Greeting = styled.span`
    font-weight: 400;
    font-size: 12px;
    line-height: 16px;

    strong {
        font-weight: 900;
    }
`;

export const LogoutButton = styled.button`
    border: 1px solid rgba(51, 51, 51, 0.2);
    width: 32px;
    height: 32px;
    box-sizing: border-box;
    padding: 9px;
    outline: none;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
`;

export const Icon = styled.img`
`;