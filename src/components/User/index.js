import React from "react";
import { useNavigate } from "react-router";
import logoutIcn from "../../assets/images/logout-icn.png";
import { Container, Greeting, Icon, LogoutButton } from "./style";

const User = ({ user }) => {
    const navigate = useNavigate();

    return (
        <Container>
            <Greeting>
                {`Bem ${user.gender === 'M' ? 'vindo' : 'vinda'},`}
                <strong>{` ${user.name}!`}</strong>
            </Greeting>
            <LogoutButton onClick={() => navigate("/logout", { replace: true })}>
                <Icon src={logoutIcn} alt="Ícone Sair" />
            </LogoutButton>
        </Container>
    )
};

export default User;