import React from "react";
import { Container } from "./style";

const ErrorMessage = ({ text }) => {
    return (
        <Container>{text}</Container>
    )
};

export default ErrorMessage;