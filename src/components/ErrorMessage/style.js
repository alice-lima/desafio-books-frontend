import React from "react";
import styled from "styled-components";

export const Container = styled.div`
    position: relative;
    background: rgba(255, 255, 255, 0.4);
    backdrop-filter: blur(2px);
    border-radius: 4px;
    padding: 16px;
    color: #fff;
    font-weight: 700;
    font-size: 16px;
    line-height: 16px;
    width: fit-content;
    margin-top: 16px;

    &::before {
        content: '';
        display:inline-block;
        position: absolute;
        border-bottom: 8px solid rgba(255, 255, 255, 0.4);
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        top: -8px;
    }
`;