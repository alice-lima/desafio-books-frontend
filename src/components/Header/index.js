import React from "react";
import Title from "../Title";
import User from "../User";
import { Container } from "./style";

const Header = ({ user }) => {
    return (
        <Container>
            <Title />
            <User user={user} />
        </Container>
    )
};

export default Header;