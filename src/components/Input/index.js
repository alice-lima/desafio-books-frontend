import React from "react";
import { Background, Column, Label, Input } from "./style";

const TextInput = ({ id, label, children, ...props }) => {
    return (
        <Background>
            <Column isLarge={!!children}>
                <Label htmlFor={id}>{label}</Label>
                <Input id={id} {...props} />
            </Column>

            {!!children && (
                <Column>
                    {children}
                </Column>
            )}

        </Background>
    )
}

export default TextInput;
