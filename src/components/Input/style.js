import styled from "styled-components";

export const Background = styled.div`
    background: rgba(0, 0, 0, 0.32);
    backdrop-filter: blur(2px);
    border-radius: 4px;
    padding: 8px 13px;
    display: flex;
    margin: 8px 0;
`;

export const Label = styled.label`
    width: 100%;
    font-style: normal;
    font-weight: 400;
    line-height: 16px;

    color: #FFFFFF;

    opacity: 0.5;
`;

export const Input = styled.input`
    background-color: transparent;
    outline: none;
    border: none;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    color: #FFFFFF;
    margin-top: 4px;

    &:-internal-autofill-selected, &:active, &:focus {
        background-color: transparent;
        color: #FFFFFF;
    }
`;

export const Column = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    flex: ${({ isLarge }) => isLarge ? 3 : 1};
`;