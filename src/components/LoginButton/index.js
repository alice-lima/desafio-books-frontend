import React from "react";
import { Button } from "./style";

const LoginButton = ({ label, ...props }) => {
    return (
        <Button {...props}>{label}</Button>
    )
}

export default LoginButton;