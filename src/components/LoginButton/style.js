import styled from "styled-components";
import { loginButton } from "../../constants/colors";

export const Button = styled.button`
    background: #FFFFFF;
    border-radius: 44px;
    outline: none;
    border: none;
    padding: 8px 20px;
    color: ${loginButton};
    cursor: pointer;
`;