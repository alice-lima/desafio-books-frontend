import React from "react";
import nextIcn from "../../assets/images/next-icn.svg";
import previousIcn from "../../assets/images/previous-icn.svg";
import { Container, Content, Button, Text } from "./style";

const Pagination = ({ activePage, numTotalPages, handleActivePage }) => {
    
    const PreviousButton = (props) => {
        return (
            <Button 
                disabled={activePage === 1} 
                onClick={() => {
                    if(activePage === 1) return;

                    handleActivePage(activePage - 1);
                }}
                {...props}
            >
                <svg>
                    <image href={previousIcn} />
                </svg>
            </Button>
        )
    };

    const NextButton = (props) => {
        return (
            <Button 
                disabled={activePage === numTotalPages} 
                onClick={() => {
                    if(activePage === numTotalPages) return;

                    handleActivePage(activePage + 1);
                }}
                {...props}
            >
                <svg>
                    <image href={nextIcn} />
                </svg>
            </Button>
        )
    };

    return (
        <Container>
            <Content>
                <PreviousButton className="show-small" />
                <Text>
                    Página{" "}
                    <strong>{activePage}</strong>
                    {" de "}
                    <strong>{numTotalPages}</strong>
                </Text>
                <PreviousButton className="show-large" />
                <NextButton />
            </Content>
        </Container>
    )
};

export default Pagination;