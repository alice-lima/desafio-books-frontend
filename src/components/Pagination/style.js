import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    justify-content: flex-end;

    @media(max-width: 768px) {
        justify-content: center;
    }
`;

export const Content = styled.div`
    display: flex;
    align-items: center;
    gap: 10px;
`;

export const Button = styled.div`
    border: 1px solid rgba(51, 51, 51, 0.2);
    box-sizing: border-box;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 32px;
    height: 32px;
    cursor: ${({ disabled }) => disabled ? "default" : "pointer"};

    svg {
        opacity: ${({ disabled }) => disabled ? 0.2 : 1};
    }

    svg, svg image {
        width: 12px;
        height: 12px;       
    }

    &.show-small {
        display: none;
    }

    &.show-large {
        display: flex;
    }

    @media(max-width: 768px) {
        &.show-small {
            display: flex;
        }

        &.show-large {
            display: none;
        }
    }
`;

export const Text = styled.div`
    font-weight: 400;
    font-size: 12px;
    line-height: 20px;

    strong {
        font-weight: 900;
    }
`;