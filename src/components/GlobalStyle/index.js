import { createGlobalStyle } from "styled-components";
import HeeboFont from "../../assets/fonts/Heebo/Heebo-VariableFont_wght.ttf";
import { primaryText } from "../../constants/colors";

export const GlobalStyle = createGlobalStyle`
    @font-face {
        font-family: "Heebo";
        src: url(${HeeboFont}) format("truetype");
    }

    * {
        font-size: 12px;
        font-family: "Heebo";
        color: ${primaryText};
    }

    html, body, #root {
        margin: 0;
        height: 100%;
    }

`;