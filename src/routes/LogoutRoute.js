import React, { useEffect } from "react";
import { Navigate } from "react-router-dom";
import { useAuth } from "../hooks/auth";
import { logout } from "../services/auth";

const LogoutRoute = () => {
    const { setLoggedUser } = useAuth();

    useEffect(() => {
        logout();
        setLoggedUser(null);
    }, [])

    return (
        <Navigate to="/login" replace />
    )
};

export default LogoutRoute;