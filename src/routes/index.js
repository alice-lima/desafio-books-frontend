import React from 'react';
import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";
import LogoutRoute from './LogoutRoute';
import PrivateRoute from './PrivateRoute';
import Books from '../pages/books';
import Login from '../pages/login';

const AppRoutes = () => {
    
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Login />} />
                <Route path="/login" element={<Login />} />
                <Route path="/books" element={
                    <PrivateRoute>
                        <Books />
                    </PrivateRoute>
                } />
                <Route path="/logout" element={<LogoutRoute />} />
            </Routes>
        </BrowserRouter>
    )
}

export default AppRoutes;