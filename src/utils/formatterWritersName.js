export const formatterWritersName = (authors) => {
    if(authors) {
        if(authors.length === 1) return authors[0]

        return authors.toLocaleString().replace(/,/gi, ', ')
    }

    return '';
};